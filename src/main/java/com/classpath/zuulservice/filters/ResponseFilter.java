package com.classpath.zuulservice.filters;


import com.classpath.zuulservice.util.FilterUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ResponseFilter extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(ResponseFilter.class);


    @Autowired
    private FilterUtils filterUtils;

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext.getCurrentContext().addZuulResponseHeader(FilterUtils.CORRELATION_ID, filterUtils.getCorrelationId());
        String correlationId = filterUtils.getCorrelationId();
        logger.info("Outbound correlation id: {}. ", correlationId);
        return null;
    }
}