package com.classpath.zuulservice.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.jws.soap.SOAPBinding;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class UserContextFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(UserContextFilter.class);

    @Override
    public  void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        UserContextHolder
                .getContext()
                .setCorrelationId(httpServletRequest.getHeader(UserContext.CORRELATION_ID));
        logger.info("Setting the correlation-id to the service {} ", UserContextHolder.getContext().getCorrelationId());
        filterChain.doFilter(servletRequest, servletResponse);
    }
}